clear all;
%Thiem gespannt station�r

%t x-achse log, s y-achse, von allen Messstellen bei 30m und 90m die
%Durchfluss, Absenkungen und Zeiten
Q=788;
r08=0.8;
r30=30;
r90=90;
s_m30=1.088;
s_m90=0.716;
s_m08=2.236;

t30=[0,0.1,0.25,0.5,0.7,1,1.4,1.9,2.33,2.8,3.36,4,5.35,6.85,8.3,8.7,10,13.1,18,27,33,41,48,59,80,95,139,181,245,300,360,480,600,728,830];
s30=[0,0.04,0.08,0.13,0.18,0.23,0.28,0.33,0.36,0.39,0.42,0.45,0.5,0.54,0.57,0.58,0.6,0.64,0.68,0.742,0.753,0.779,0.793,0.819,0.855,0.873,0.915,0.935,0.966,0.990,1.007,1.050,1.053,1.072,1.088];
t90=[0,1.5,2,2.16,2.66,3,3.5,4,4.33,5.5,6,7.5,9,13,15,18,25,30,40,53,60,75,90,105,120,150,180,248,301,363,422,542,602,680,785,845];
s90=[0,0.015,0.021,0.023,0.044,0.054,0.075,0.090,0.104,0.133,0.153,0.178,0.206,0.250,0.275,0.305,0.3448,0.364,0.404,0.429,0.444,0.467,0.494,0.507,0.528,0.550,0.569,0.593,0.614,0.636,0.657,0.679,0.688,0.701,0.718,0.716];
sm30=s30.*(-1);
sm90=s90.*(-1);
t200=[0,66,127,185,251,305,366,430,606,780];
s200=[0,0.089,0.138,0.165,0.186,0.196,0.207,0.214,0.227,0.250];
sm200=s200.*(-1);

%plot
semilogx(t30,sm30,'-o');
hold on;
semilogx(t90,sm90,'-o');
axis([10^-1 10^3 -1.2 0.4]);
semilogx(t200,sm200,'-o');


%Berechnungen
T1= ((Q*2.30)/(2*pi*(sm30-sm90)))*log10(r90/r30)
T2= ((Q*2.30)/(2*pi*(sm08-sm90)))*log10(r90/r08)
T3= ((Q*2.30)/(2*pi*(sm08-sm30)))*log10(r30/r08)
%Mittelwert
Tm=(T1+T2+T3)/3

%%
clear all;
%Thiem gespannt station�r
% r x-achse log, sm y-achse, jeweils von allen Messstellen
%Durchluff, Radien und Absenkungen
Q=788;
r=[log10(0.8),log10(30),log10(90)];
sm=[2.236, 1.088, 0.716];

%plot
hold on
[p2,S] = polyfit(r,sm,1);
[y_fit,delta] = polyval(p2,r,S);
A = [ones(size(sm)),r];
c = A\sm;
y = A*c;
semilogx(r,y,'o')
semilogx(r,y_fit,'-')
txt1=['y= ',(num2str(p2(1))),'x+ ',num2str(p2(2))];
text(1,0.9,txt1);

%Berechnung
T= (2.3*Q)/(2*pi*(-p2(1)))

%%
clear all;
%Theis gespannt instation�r
clear all;
f=figure

%Zeiten und Radien
t30=[0,0.1,0.25,0.5,0.7,1,1.4,1.9,2.33,2.8,3.36,4,5.35,6.85,8.3,8.7,10,13.1,18,27,33,41,48,59,80,95,139,181,245,300,360,480,600,728,830];
t90=[0,1.5,2,2.6,2.66,3,3.5,4,4.33,5.5,6,7.5,9,13,15,18,25,30,40,53,60,75,90,105,120,150,180,248,301,363,422,542,602,680,785];
r90=90;
r30=30;

%t/r^2(30)
x1=t30/r30^2;
%t/r^2(90)
x3=t90/r90^2;

%s30
y1=[0,0.04,0.08,0.13,0.18,0.23,0.28,0.33,0.36,0.39,0.42,0.45,0.5,0.54,0.57,0.58,0.6,0.64,0.68,0.742,0.753,0.779,0.793,0.819,0.855,0.873,0.915,0.935,0.966,0.99,1.007,1.05,1.053,1.072,1.088];
%s90
y3=[0,0.015,0.021,0.023,0.044,0.054,0.075,0.09,0.104,0.133,0.153,0.178,0.206,0.250,0.275,0.305,0.348,0.364,0.404,0.429,0.444,0.467,0.494,0.507,0.528,0.55,0.569,0.593,0.614,0.636,0.657,0.679,0.688,0.701,0.718];

% plot
a=0.1:5:400;
b=0.00000000001*a+1

loglog(x1,y1,'-.')
hold on
loglog(x3,y3,'.')

ax1=gca;
axis([10^-5 1 10^-3 1]);

xlabel('t/r^2 in min/m^2');
ylabel('s in Metern');

ax1.XColor='r';
ax1.YColor='r';
ax1_pos=ax1.Position;
ax2=axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'Color','none');

u=logspace(-7,1,100);
z=1./u;
syms n

y2=-0.5772-log(u)+symsum((-1)^(n+1)*u.^(n)/(n*factorial(n)),n,1,300);

line(z,y2,'parent',ax2,'color','k');

set(gca,'XScale','log')
set(gca,'YScale','log')
axis([10^-1 10^4 10^-2 10]);
xlabel('1/u');
ylabel('W(u)');
%Berechnungen nach ablesen der Werte und Eintragung in Zeile 123 und 124
%W(u)=1;
%1/u=10;
%(t/r^2)_a= ;
%s_a= ;
%k_f=(Q/(4*pi*s_a))*W(u);
%S=(4*k_f*(t/r^2)_a)/(1/u);

%%
%Jacob gespannt instation�r
%r ist konstant!
%ein Piezometer
%t x-achse log, s y-achse

clear all;
%Durchfluss, Zeiten und Absenkungen
tl30=[log10(1),log10(1.4),log10(1.9),log10(2.33),log10(2.8),log10(3.36),log10(4),log10(5.35),log10(6.85),log10(8.3),log10(8.7),log10(10),log10(13.1),log10(18),log10(27),log10(33),log10(41),log10(48),log10(59),log10(80),log10(95)];
s1=[0.23,0.28,0.33,0.36,0.39,0.42,0.45,0.5,0.54,0.57,0.58,0.6,0.64,0.68,0.742,0.753,0.779,0.793,0.819,0.855,0.873];
t90=[log10(1.5),log10(2),log10(2.16),log10(2.66),log10(3),log10(3.5),log10(4),log10(4.33),log10(5.5),log10(6),log10(7.5),log10(9),log10(13),log10(15),log10(18),log10(25),log10(30),log10(40),log10(53),log10(60),log10(75),log10(90),log10(105),log10(120),log10(150),log10(180),log10(248),log10(301),log10(363),log10(422),log10(542),log10(602),log10(680),log10(785),log10(845)];
s90=[0.015,0.021,0.023,0.044,0.054,0.075,0.090,0.104,0.133,0.153,0.178,0.206,0.250,0.275,0.305,0.3448,0.364,0.404,0.429,0.444,0.467,0.494,0.507,0.528,0.550,0.569,0.593,0.614,0.636,0.657,0.679,0.688,0.701,0.718,0.716];
Q=788;
r30=30;
r90=90;

%plot1
hold on
[p2,S] = polyfit(tl30,s1,1);
[y_fit,delta] = polyval(p2,tl30,S);
A = [ones(size(s1)),tl30];
c = A\s1;
y = A*c;
semilogx(tl30,y,'o')
semilogx(tl30,y_fit,'-')
txt1=['y_1= ',(num2str(p2(1))),'x+ ',num2str(p2(2))];
text(1,0.9,txt1);

%plot2
hold on
[p3,S]=polyfit(t90,s90,1);
[y_fit,delta]=polyval(p3,t90,S);
A=[ones(size(s90)),t90];
c=A\s90;
y=A*c;
semilogx(t90,y,'o')
semilogx(t90,y_fit,'-')
txt2=['y_2=',(num2str(p3(1))),'x+',num2str(p3(2))];
text(1,0.7,txt2);


%Berechnungen
T1= (2.3*Q)/(4*pi*p2(1));
T2= (2.3*Q)/(4*pi*p3(1));
t_01=10^(-(p2(2)/p2(1)));
t_02=10^(-(p3(2)/p3(1)));
S1=(2.25*T1*t_01)/((r30^2)*1440);
S2=(2.25*T2*t_02)/((r90^2)*1440);
%Bedingung
ttest1=0.001
ttest2=11/1440;
u1=r30^2*S1/(4*T1*ttest1);
u2=r90^2*S2/(4*T2*ttest2);
%if u<0.1 -> Bedingung erf�llt

%%
clear all;
%Jacob gespannt instation�r
%t konstant, Variable t aussuchen
%r x-achse log, s y-achse
%Durchfluss, Absenkungen, Radien und Zeiten
Q=788;
r=[log10(30),log10(90)];
t30=[0,0.1,0.25,0.5,0.7,1,1.4,1.9,2.33,2.8,3.36,4,5.35,6.85,8.3,8.7,10,13.1,18,27,33,41,48,59,80,95,139,181,245,300,360,480,600,728,830];
s30=[0,0.04,0.08,0.13,0.18,0.23,0.28,0.33,0.36,0.39,0.42,0.45,0.5,0.54,0.57,0.58,0.6,0.64,0.68,0.742,0.753,0.779,0.793,0.819,0.855,0.873,0.915,0.935,0.966,0.990,1.007,1.050,1.053,1.072,1.088];
t90=[0,1.5,2,2.16,2.66,3,3.5,4,4.33,5.5,6,7.5,9,13,15,18,25,30,40,53,60,75,90,105,120,150,180,248,301,363,422,542,602,680,785,845];
s90=[0,0.015,0.021,0.023,0.044,0.054,0.075,0.090,0.104,0.133,0.153,0.178,0.206,0.250,0.275,0.305,0.3448,0.364,0.404,0.429,0.444,0.467,0.494,0.507,0.528,0.550,0.569,0.593,0.614,0.636,0.657,0.679,0.688,0.701,0.718,0.716];
%Interpolation f�r z.B: t=140
t140=140;
s140=[interp1(t30,s30,140),interp1(t90,s90,140)];

%plot
hold on
[p2,S] = polyfit(r,s140,1);
[y_fit,delta] = polyval(p2,r,S);
A = [ones(size(s140)),r];
c = A\s140;
y = A*c;
semilogx(r,y,'o')
semilogx(r,y_fit,'-')
txt1=['y= ',(num2str(p2(1))),'x+ ',num2str(p2(2))];
text(1.8,0.9,txt1);

r_0=10^(-(p2(2)/p2(1)));


%Berechnungen
k_f=(2.3*Q)/(2*pi*(-p2(1)))
S=(2.25*k_f*t140)/(1440*(r_0)^2)

%%
clear all;
%Jacob gespannt instation�r
%t/r^2 x-achse log, s y-achse
%Durchfluss, Radien, Absenkungen und Zeiten
Q=788;
r30=30;
r90=90;
t30=[0.1,0.25,0.5,0.7,1,1.4,1.9,2.33,2.8,3.36,4,5.35,6.85,8.3,8.7,10,13.1,18,27,33,41,48,59,80,95,139,181,245,300,360,480,600,728,830];
s30=[0.04,0.08,0.13,0.18,0.23,0.28,0.33,0.36,0.39,0.42,0.45,0.5,0.54,0.57,0.58,0.6,0.64,0.68,0.742,0.753,0.779,0.793,0.819,0.855,0.873,0.915,0.935,0.966,0.990,1.007,1.050,1.053,1.072,1.088,0.015,0.021,0.023,0.044,0.054,0.075,0.090,0.104,0.133,0.153,0.178,0.206,0.250,0.275,0.305,0.3448,0.364,0.404,0.429,0.444,0.467,0.494,0.507,0.528,0.550,0.569,0.593,0.614,0.636,0.657,0.679,0.688,0.701,0.718,0.716];
t90=[1.5,2,2.16,2.66,3,3.5,4,4.33,5.5,6,7.5,9,13,15,18,25,30,40,53,60,75,90,105,120,150,180,248,301,363,422,542,602,680,785,845];
s90=[0.015,0.021,0.023,0.044,0.054,0.075,0.090,0.104,0.133,0.153,0.178,0.206,0.250,0.275,0.305,0.3448,0.364,0.404,0.429,0.444,0.467,0.494,0.507,0.528,0.550,0.569,0.593,0.614,0.636,0.657,0.679,0.688,0.701,0.718,0.716];
tra=t30/r30^2;
trb=t90/r90^2;
%logarithmieren der Werte auf der x-achse
tr30=[log10(tra(1)),log10(tra(2)),log10(tra(3)),log10(tra(4)),log10(tra(5)),log10(tra(6)),log10(tra(7)),log10(tra(8)),log10(tra(9)),log10(tra(10)),log10(tra(11)),log10(tra(12)),log10(tra(13)),log10(tra(14)),log10(tra(15)),log10(tra(16)),log10(tra(17)),log10(tra(18)),log10(tra(19)),log10(tra(20)),log10(tra(21)),log10(tra(22)),log10(tra(23)),log10(tra(24)),log10(tra(25)),log10(tra(26)),log10(tra(27)),log10(tra(28)),log10(tra(29)),log10(tra(30)),log10(tra(31)),log10(tra(32)),log10(tra(33)),log10(tra(34)),log10(trb(1)),log10(trb(2)),log10(trb(3)),log10(trb(4)),log10(trb(5)),log10(trb(6)),log10(trb(7)),log10(trb(8)),log10(trb(9)),log10(trb(10)),log10(trb(11)),log10(trb(12)),log10(trb(13)),log10(trb(14)),log10(trb(15)),log10(trb(16)),log10(trb(17)),log10(trb(18)),log10(trb(19)),log10(trb(20)),log10(trb(21)),log10(trb(22)),log10(trb(23)),log10(trb(24)),log10(trb(25)),log10(trb(26)),log10(trb(27)),log10(trb(28)),log10(trb(29)),log10(trb(30)),log10(trb(31)),log10(trb(32)),log10(trb(33)),log10(trb(34)),log10(trb(35))];
tr90=[log10(trb(1)),log10(trb(2)),log10(trb(3)),log10(trb(4)),log10(trb(5)),log10(trb(6)),log10(trb(7)),log10(trb(8)),log10(trb(9)),log10(trb(10)),log10(trb(11)),log10(trb(12)),log10(trb(13)),log10(trb(14)),log10(trb(15)),log10(trb(16)),log10(trb(17)),log10(trb(18)),log10(trb(19)),log10(trb(20)),log10(trb(21)),log10(trb(22)),log10(trb(23)),log10(trb(24)),log10(trb(25)),log10(trb(26)),log10(trb(27)),log10(trb(28)),log10(trb(29)),log10(trb(30)),log10(trb(31)),log10(trb(32)),log10(trb(33)),log10(trb(34)),log10(trb(35))];

%plot
hold on
[p2,S] = polyfit(tr30,s30,1);
[y_fit,delta] = polyval(p2,tr30,S);
A = [ones(size(s30)),tr30];
c = A\s30;
y = A*c;
semilogx(tr30,y,'o')
semilogx(tr30,y_fit,'-')
txt1=['y= ',(num2str(p2(1))),'x+ ',num2str(p2(2))];
text(-3.5,1,txt1);
semilogx(tr90,s90,'o')

tr_01=10^(-(p2(2)/p2(1)));

%Berechnung
k_f1=(2.3*Q)/(4*pi*p2(1));
S1=2.25*k_f1*tr_01/1440;
%%
clear all;
%Anisotropie Auswertung nach Hantush
%Zeiten (logarithmiert), Absenkungen, Durchfluss und Entfernungen der
%Messstellen zum Entnahmebrunnen
t=[0.5,1,2,3,4,6,8,10,15,20,30,40,50,60,90,120,150,180,240,300,360,480,720];
tlog=[log10(t(1)),log10(t(2)),log10(t(3)),log10(t(4)),log10(t(5)),log10(t(6)),log10(t(7)),log10(t(8)),log10(t(9)),log10(t(10)),log10(t(11)),log10(t(12)),log10(t(13)),log10(t(14)),log10(t(15)),log10(t(16)),log10(t(17)),log10(t(18)),log10(t(19)),log10(t(20)),log10(t(21)),log10(t(22)),log10(t(23))];
%Zur Korrektur der Ausgleichsgeraden werden Anfangspunkte der Absenkungen
%in s1 und s2 herausgenommen
s11=[0.335,0.591,0.911,1.082,1.215,1.405,1.549,1.653,1.853,2.019,2.203,2.344,2.450,2.541,2.750,2.901,2.998,3.075,3.235,3.351,3.438,3.587,3.784];
s1=[0.591,0.911,1.082,1.215,1.405,1.549,1.653,1.853,2.019,2.203,2.344,2.450,2.541,2.750,2.901,2.998,3.075,3.235,3.351,3.438,3.587,3.784];
s22=[0.153,0.343,0.611,0.762,0.911,1.089,1.225,1.329,1.531,1.677,1.853,2.019,2.123,2.210,2.416,2.555,2.670,2.750,2.901,2.998,3.118,3.247,3.455];
s2=[0.762,0.911,1.089,1.225,1.329,1.531,1.677,1.853,2.019,2.123,2.210,2.416,2.555,2.670,2.750,2.901,2.998,3.118,3.247,3.455];
s3=[0.492,0.762,1.089,1.284,1.419,1.609,1.757,1.853,2.071,2.210,2.416,2.555,2.670,2.750,2.963,3.118,3.218,3.310,3.455,3.565,3.649,3.802,3.996];
Q=1086;
r1=28.3;
r2=sqrt(9^2+33.5^2);
r3=sqrt(5.2^2+19.3^2);
plot(tlog,s11,'o',tlog,s22,'o')
%plot1
hold on
[p1,S] = polyfit(tlog,s11,1);
[y_fit,delta] = polyval(p1,tlog,S);
A = [ones(size(s11)),tlog];
c = A\s11;
y = A*c;
f1=semilogx(tlog,y,'o');
f2=semilogx(tlog,y_fit,'-');
txt1=['y_1= ',(num2str(p1(1))),'x+ ',num2str(p1(2))];
text(1,1.5,txt1);

%Berechnungen/Auswertung
Te1= (2.3*Q)/(4*pi*p1(1));
t_01=10^(-(p1(2)/p1(1)));
S1=(2.25*Te1*t_01)/((r1^2)*1440);

%Bedingung
ttest1=0.001;
u1=r1^2*S1/(4*Te1*ttest1);
%if u<0.1 -> Bedingung erf�llt

%plot2
hold on
[p2,S] = polyfit(tlog,s22,1);
[y_fit,delta] = polyval(p2,tlog,S);
A = [ones(size(s22)),tlog];
c = A\s22;
y = A*c;
f3=semilogx(tlog,y,'o');
f4=semilogx(tlog,y_fit,'-');
txt2=['y_2= ',(num2str(p2(1))),'x+ ',num2str(p2(2))];
text(0,0.2,txt2);

%Berechnungen/Auswertung
Te2= (2.3*Q)/(4*pi*p2(1));
t_02=10^(-(p2(2)/p2(1)));
S2=(2.25*Te2*t_02)/((r2^2)*1440);

%Bedingung
ttest2=0.001;
u2=r2^2*S2/(4*Te2*ttest2);
%if u<0.1 -> Bedingung erf�llt

%plot3
hold on
[p3,S] = polyfit(tlog,s3,1);
[y_fit,delta] = polyval(p3,tlog,S);
A = [ones(size(s3)),tlog];
c = A\s3;
y = A*c;
f5=semilogx(tlog,y,'o');
f6=semilogx(tlog,y_fit,'-');
txt3=['y_3= ',(num2str(p3(1))),'x+ ',num2str(p3(2))];
text(1.2,3.6,txt3);
legend([f1 f2 f3 f4 f5 f6])
%Berechnungen/Auswertung
Te3= (2.3*Q)/(4*pi*p3(1));
t_03=10^(-(p3(2)/p3(1)));
S3=(2.25*Te3*t_03)/((r3^2)*1440);

%Bedingung
ttest3=0.001;
u3=r3^2*S3/(4*Te3*ttest3);
%if u<0.1 -> Bedingung erf�llt



%Berechnung des Winkels und der Transmissivit�ten in Hauptrichtungsachsen
%sowie der Speicherkoeffizienten
Tem=(Te1+Te2+Te3)/3;
ST1=2.25*t_01/(r1^2*1440);
ST2=2.25*t_02/(r2^2*1440);
ST3=2.25*t_03/(r3^2*1440);

alpha1=0;
alpha2=75;
alpha3=196;
a1=1;
a2=ST2/ST1;
a3=ST3/ST1;
a22=a2-1;
a33=a3-1;
d1=sind(alpha2).^2;
d2=sind(alpha3).^2;
d3=sind(2*alpha2);
d4=sind(2*alpha3);
tanphi=-2*((a33*d1-a22*d2)/(a33*d3-a22*d4));
teta=atand(tanphi)/2;
teta1=-teta;
teta2=teta+90;
m1=(a2*cosd(teta1)^2-cosd(teta1+alpha2)^2)/(sind(teta1+alpha2)^2-a2*sind(teta1)^2);
m2=(a2*cosd(teta2)^2-cosd(teta2+alpha2)^2)/(sind(teta2+alpha2)^2-a2*sind(teta2)^2);

TY=Tem/sqrt(m1);
TX=m1*TY;
T1=TX/(cosd(teta1+alpha1)^2+m1*sind(teta1+alpha1)^2);
T2=TX/(cosd(teta1+alpha2)^2+m1*sind(teta1+alpha2)^2);
T3=TX/(cosd(teta1+alpha3)^2+m1*sind(teta1+alpha3)^2);
S11=ST1*T1;
S22=ST2*T2;
S33=ST3*T3;

%%
%Skript f�r die Ausrichtung der Transmissivit�ten nach Hantush
%hierf�r das vorherige und folgende Skript zusammen verwenden

%Standorte der Messstellen von dem Entnahmebrunnen ausgehend
x1=28.3; y1=0; x2=9; y2=33.5; x3=-19.3; y3=-5.2;

%plot
plot (x1,y1,'o')
hold on
plot([-20 30],[0 0])
plot([0 0],[-10 35])
plot (x2,y2,'o')
plot (x3,y3,'o')
p1=plot ([x1 0],[y1 0]),
p2=plot ([x2 0],[y2 0]);
p3=plot ([x3 0],[y3 0]);
x4=15;y4=0;x5=0;y5=15;
%Winkel
teta=teta1;
%gedrehte Achsen
x6=x4*cosd(teta)-y4*sind(teta);
y6=x4*sind(teta)+y4*cosd(teta);
x7=x5*cosd(teta)-y5*sind(teta);
y7=x5*sind(teta)+y5*cosd(teta);
p7=plot([x7 0],[y7 0]);
p8=plot([x6 0],[y6 0]);
legend([p1 p2 p3 p7 p8])
%Winkel
teta=teta2;
%verschobene Achse
x66=x4*cosd(teta)-y4*sind(teta);
y66=x4*sind(teta)+y4*cosd(teta);
x77=x5*cosd(teta)-y5*sind(teta);
y77=x5*sind(teta)+y5*cosd(teta);
p77=plot([x77 0],[y77 0]);
p88=plot([x66 0],[y66 0]);
legend([p1 p2 p3 p7 p8 p77 p88])

%%
%Anisotropie Auswertung nach Papadopulos f�r late-time Daten
clear all;
%Durchfluss, M�chtigkeit, Zeiten, Radien und Absenkung
Q=43;
M=7.6;
deltas=0.78
t01=2.35/1440;
t02=1.3/1440;
t03=2/1440;

%sqrt(TxxTyy-Txy^2)
A=2.303*Q/(4*pi*deltas);
a=A^2;

%Radien/Koordinaten der Brunnen
x1=9.69;
y1=11.84;
x2=-14.42;
y2=-2.04;
x3=11.59;
y3=-9.17;

%Werte f�r Matrix A
a1=y1^2;
a2=x1^2;
a3=2*x1*y1;
b1=y2^2;
b2=x2^2;
b3=2*x2*y2;
c1=y3^2;
c2=x3^2;
c3=2*x3*y3;

%Matrix A
A1=[a1,a2,-a3;b1,b2,-b3;c1,c2,-c3];

%Werte f�r Vektor D
d1=2.25*t01*a;
d2=2.25*t02*a;
d3=2.25*t03*a;

%Vektor D
D=[d1;d2;d3];

%LGS: A*x=D
%L�sung LGS
x=A1\D;
x9=x(1)*x(2);
x33=x(3)^2;
S=sqrt((x9-x33)/a);
Tx=x(1)/S;
Ty=x(2)/S;
Tz=x(3)/S;
Tsig=0.5*(Tx+Ty+((Tx-Ty)^2+4*Tz^2)^(0.5));
Tnan=0.5*(Tx+Ty-((Tx-Ty)^2+4*Tz^2)^(0.5));
Ratio=Tsig/Tnan;
teta=atand((Tsig-Tx)/Tz);
Te=sqrt(Tsig*Tnan);
Ke=Te/M;
Ss=S/M;

%Brunnenstandorte, x-y Achse und sig-nan-Achse
f1=plot(x1,y1,'o',x2,y2,'o',x3,y3,'o');
hold on
f2=plot([-20 20],[0 0]);
f3=plot([0 0],[-20 20]);
f4=plot([x1 0],[y1 0]);
f5=plot([x2 0],[y2 0]);
f6=plot([x3 0],[y3 0]);
x4=15.2; y4=0;
x5=0; y5=15.2;
x6=x4*cosd(teta)-y4*sind(teta);
y6=x4*sind(teta)+y4*cosd(teta);
x7=x5*cosd(teta)-y5*sind(teta);
y7=x5*sind(teta)+y5*cosd(teta);
f7=plot([x7 0],[y7 0]);
f8=plot([x6 0],[y6 0]);
legend([f4 f5 f6 f7 f8])
%%
%Anisotropie Auswertung nach Papadopulos f�r early-time Daten
clear all;
%Durchfluss, M�chtigkeit, Zeiten, Radien und Absenkung
Q=43;
M=7.6;
deltas=0.586
t01=0.413/1440;
t02=0.232/1440;
t03=0.344/1440;

%sqrt(TxxTyy-Txy^2)
A=2.303*Q/(4*pi*deltas);
a=A^2;

%Radien/Koordinaten der Brunnen
x1=9.69;
y1=11.84;
x2=-14.42;
y2=-2.04;
x3=11.59;
y3=-9.17;

%Werte f�r Matrix A
a1=y1^2;
a2=x1^2;
a3=2*x1*y1;
b1=y2^2;
b2=x2^2;
b3=2*x2*y2;
c1=y3^2;
c2=x3^2;
c3=2*x3*y3;

%Matrix A
A1=[a1,a2,-a3;b1,b2,-b3;c1,c2,-c3];

%Werte f�r Vektor D
d1=2.25*t01*a;
d2=2.25*t02*a;
d3=2.25*t03*a;

%Vektor D
D=[d1;d2;d3];

%LGS: A*x=D
x=A1\D;

S=sqrt(((x(1)*x(2))-x(3)^2)/a);
Tx=x(1)/S;
Ty=x(2)/S;
Tz=x(3)/S;
Tsig=0.5*(Tx+Ty+((Tx-Ty)^2+4*Tz^2)^(0.5));
Tnan=0.5*(Tx+Ty-((Tx-Ty)^2+4*Tz^2)^(0.5));
Ratio=Tsig/Tnan;
teta=atand((Tsig-Tx)/Tz);
Te=sqrt(Tsig*Tnan);
Ke=Te/M;
Ss=S/M;

%Brunnenstandorte, x-y-Achse und sig-nan-Achse
plot(x1,y1,'o',x2,y2,'o',x3,y3,'o')
hold on
plot([-20 20],[0 0])
plot([0 0],[-20 20])
plot([x1 0],[y1 0])
plot([x2 0],[y2 0])
plot([x3 0],[y3 0])
x4=15.2; y4=0;
x5=0; y5=15.2;
x6=x4*cosd(teta)-y4*sind(teta);
y6=x4*sind(teta)+y4*cosd(teta);
x7=x5*cosd(teta)-y5*sind(teta);
y7=x5*sind(teta)+y5*cosd(teta);
plot([x7 0],[y7 0])
plot([x6 0],[y6 0])










