# Weh2018a

A. Weh, BSc thesis, 2018: Pumpversuche zur Bestimmung hydrogeologischer Parameter 

Summary
=======

This is the Matlab code containing the code for producing the results
published in:

A. Weh<br>
Pumpversuche zur Bestimmung hydrogeologischer Parameter<br>
Bachelor's thesis, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 2018.


Installation
============

You can download the module or the matlab script and execute it. Be aware that the script is documented and several parts depend on each other.


Output
======

Run the program according to the inline comments. 
Make sure to run several parts only after establishing the correct data.

Used Versions and Software
==========================
Matlab R2018a was used